﻿# reddit-wow-minecraft-classifier
The classifier is trained over Reddit rules by using a linear SVM implementation along with TF-IDF features. Basic preprocessing (punctuation, whitespace, stop words, lowercasing, stemming) is applied before the training phase. 

## Classification Tasks
*  Detect institutional statements (IS) 
* Classify each IS as either regulatory or constitutive, either rule, norm, strategy (RNS)
* Assign the eight binary labels of the Rule Typology
* Use Reddit-trained classifier to label all uncoded files

## Development Setup
```bash
pip install bs4
pip install numpy
pip install pandas
pip install gensim
pip install notebook
pip install -U scikit-learn
```
